# notes

## Personal Projects
---------
# React app
-- Expense tracker
-- Stock trading app

# Blockchain App
-- Voting System
-- Stock related app
-- Security app
-- Online shopping mart


# If Possible
-- Personal crypto

# Node App
-- Advanced otp authentication

## Learning
---------
-- Kubernetes
-- Docker
-- Open Source
